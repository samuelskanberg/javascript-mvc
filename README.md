# Javascript MVC

A simple web app using MVC design and vanilla javascript. 

It is deployed at [github pages](https://samuelskanberg.gitlab.io/javascript-mvc/).

## To run locally

```bash
python3 -m http.server --directory src
```

Now surf to http://localhost:8000